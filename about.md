---
layout: page
title: About Behind KDE
css-include: /css/main.css
sorted: 4
---

## How it all started

Way back in the year 2000, [Tink Bastian](https://behindkde.org/node/157) created a series of interviews with people who contribute to KDE.
She wanted to show the personal interest of these people, their kicks, motivations and passions:
the interviews are therefore not only about KDE, but also delve into the personal life of the person behind the online persona.
All interviews in Series 1 were produced by [Tink Bastian](https://behindkde.org/node/157).
Other commitments in her life sadly left her with no time to work on this series,
and so in 2005 she asked the [Dutch KDE community](http://kde.nl/) to maintain and continue the series, which became Series 2.

2007 brought Series 3, produced by [Danny Allen](mailto:danny@behindkde.org), of the weekly [KDE Commit-Digest](http://commit-digest.org/).

New for 2008 is Series 4, with interviews conducted by A. L. Spehr.

In 2010 [Tom Albers](mailto:toma@kde.org) decided that it was time for some new interviews and interviewed KDE Sysadmin and the people directly involved around the sysadmin team.

## Credits

Firstly, big thanks must go to Tink Bastian for having the foresight and perseverance to create the original concept and series.

Series 1: Tink Bastian

Series 2: Tijmen Baarda, Fabrice Mous, Jonathan Riddell

Series 3: Danny Allen

Series 4: A. L. Spehr

Series 5: Tom Albers
