---
layout: post
title: "David Faure"
date: 2010-09-22 00:01:00
submitter: toma
---

This weeks interview in this series of behind KDE is with a well known old-timer. We all know that he is the guru of kdelibs, but do you know what he does for kdesysadmin? Or should I say 'did'? Meet the guy who developed for KDE while some of you were not even born. Meet the one who moved files manually in the cvs era and see how Charm and Jazz play an important role in his life. Meet David Faure!

## Can you give an introduction of yourself?

I'm french, aged 34, and Qt/KDE is both my hobby and my job :-) Well, I also play jazz piano, as a secondary hobby.

## Can you tell us what you do for a living?

I am lucky enough to be able to work on KDE for a living, 20 hours per week. I work for KDAB on Qt-related projects, and I'm sponsored by Nokia for the KDE work.

## Can you tell us what you do for KDE?

KDE developer since 1998, maintainer of Konqueror and large portions of the KDE libraries. In addition to development (mostly bugfixing, occasionally new features), I review many patches from others, and help people with specific development questions on IRC. As such a long-time contributor to KDE, people often come to me with questions about why things in kdelibs were done in a certain way, so despite my usually bad memory, I end up playing the role of the "memory of the project" a little bit :) Old-timer joke: Radej's beer wasn't cold while he wrote kmenubar...

## How do you divide your time between all the different things that you do?

With difficulty. At KDAB we use Charm (available in KDE playground) to track the time spend on different projects, so I can add up the "KDE time" every week and aim at 20 hours on average, but inside of that KDE time, I often have trouble deciding what to give priority to. I keep a list of important bugs and issues to look at, but that is very often interrupted by IRC or private-mail requests.

## You are an important part of the sysadmin team, can you explain in more detail what you do?

I became a sysadmin a long time ago (around 2000 maybe) because Coolo wanted to go on vacations so I suggested to take over his responsibilities during that time. Back then it included moving files by hand on the CVS server, whenever a developer wanted to rename a file :-) Then I have been mostly doing the day-to-day tasks (creating svn accounts, email aliases, and mailing-lists) for a very long time. And the occasional "restarting a service on a server" when necessary. Nowadays I mostly do mailing-lists only, and giving necessary access to the newer member of the teams.

## You indicated in the past that you want to do less in the sysadmin area, why on earth makes you say that?

Well, I am no good at it :-)
I never had any training or experience with actual sysadmin stuff, I only gave a hand as a matter of necessity. And because someone had to do the boring day- to-day tasks, and since I was sponsored for working on KDE, it seemed fair that I would do that. Nowadays, however, the number of sysadmins has increased greatly, some tasks are more automated, and generally it seems that I'm more useful to KDE when fixing bugs or helping developers on IRC, than when doing sysadmin tasks that others can do better. The main reason this has lasted so long, is that integrating new volunteers is difficult in the sysadmin area, there's a question of trust, and how do we trust someone who hasn't been visible inside KDE for a long time -- especially when we want the KDE developers to spend time coding instead :). It seems we have finally solved this problem now, with the integration of experienced sysadmins, who have shown great commitment to KDE.

## The coming months stuff will change due to the transition to git. Can you explain the changes and your role in it?

I have actually stayed out of that, seeing how other people are much more experienced with git. I am gaining some git experience with Qt merge requests, so I have simply, on occasion, suggested to the rest of the team how we should not do things in the future KDE git :-)

## What are the areas where KDE and its software really shines in your opinion?

Since I'm mainly working at the level of the KDE libraries, I will freely interpret this question as "the KDE development platform" :). I think the KDE platform has achieved a nice level of providing (with Qt, of course) everything that is necessary to develop applications easily and in a consistent way. There is a high degree of code sharing between KDE applications, and the framework provided by kdelibs makes it easy to provide new features in all KDE applications without changing a single line of code in the applications, or in some cases, adding a single line to enable the feature.

Of course the KDE workspace and the KDE applications shine as well, but I prefer to let others advertise these.

## Do you have a vision, like where do you want KDE in general to be in 5 years and sysadmin in particular?

I always say, there are enough people with visions already, I prefer being among those who actually move things forward -- and we need more of those, rather than more individual visions. But I certainly hope that KDE will have 5 times more developers, 50 times more users, and 5 times less bugs, by 2015 :-)

My vision for sysadmin was a group of experienced sysadmins with time for KDE, and my wish seems to have been granted earlier this year, so I'll be able to retire from it and concentrate on development instead.
