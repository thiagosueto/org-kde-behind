---
layout: post
title: "Behind KDE revived"
date: 2050-01-01 00:01:00
submitter: "KDE Web team"
---

## The Revival of Behind KDE

Hello all, and welcome to the new [behind.kde.org](https://behind.kde.org) website!

This website has been successfully redesigned in Jekyll by the KDE Web team. We've been meaning to revisit its concept for quite a while on both KDE Promo and KDE Web, as getting to know more about contributors and maintainers is interesting, engaging and ─ especially ─ human.

Very often in open source we see only the machine side of things: code that either runs or fails, technical issues, practical results, software efficiency. Moreover, in today's society, software is generally perceived as products, tools, replaceable things in general, and in most cases they are one or the other. However, the humans behind the machine do not work like that, no matter how much you try to act like one.

Humans have subtleties other humans can identify with; they sometimes make mistakes, and that's fine; they care about the software they develop, in certain cases even considering it a child project to be cared for extensively. It is nice when contributors are able to have someone to pass the torch to once they stop contributing ─ but that does not mean they are truly replaceable. What they do and have done are valuable things, and their participation in the KDE community is never to be replaced.

It is honestly amazing to see those people working on their favorite software with passion and see how similar they are to ourselves. This very much means they were in the same spot as you currently are, and that you can be part of the people contributing to improve it and have it available to others, be it by code, design, translation, websites, documentation, promotion and, generally, any other task one might want to do.

With open source, it's about what people *want* to do. Contributors are very often also users of the software they contribute to, and if they want certain things on that software, they make efforts to achieve this in some way. This *desire* is what drives open source, and as such contributing belongs to the things one *wants* to do, things one *likes* to do.

It only makes sense to be curious about the rest: What else do those people like? What do they want to do next? What interests them? What plans do they have?

We hope to continue making such interviews as we used to, so we can keep sating this curiosity.

── The KDE Community
