---
layout: post
title: "KDE and the Masters of the Universe 0x0002"
date: 2010-05-21 00:01:00
---

[This week on KDEMU](http://webbaverse.com/media/kdemu-0x0002) we introduce Mike Arthur one of the co-hosts of KDEMU, we talk about KDE on Mac OSX, his plans for rent-a-child and his love for David Faure! Don’t forget to comment in the show page or follow @gamaral and @mikearthur on Twitter and let us have it!
