---
layout: post
title: "Vishesh Handa"
date: 2012-09-18 00:01:00
submitter: jrowe
---

In this episode of Behind KDE, we meet young and enthusiastic developer Vishesh Handa, the new driving force behind the semantic capabilities in KDE Workspaces. Vishesh talks about his beginnings with KDE development as well as his current fulltime job of improving Nepomuk.

Name: Vishesh Handa
Age: 22
Location: New Delhi, India
Project: Nepomuk; KDE semantic stack

## Could you start by telling us a little about yourself?

Hi. My name is Vishesh Handa, known as vHanda in the online world. I'm a computer science graduate from Delhi, India.

## What do you do for a living?
Until about 2 months back I was a student, studying for my bachelors in computer science. Now that I've graduated, I'm working on [Nepomuk](http://userbase.kde.org/Nepomuk) full time sponsored by [Blue Systems](http://blue-systems.com/).

## What do you do for KDE?

I predominantly work on KDE's semantic stack. This mostly includes Nepomuk and all of the applications that use Nepomuk. Apart from coding, I have conducted some minor workshops to get students interested in KDE development.

## How did you get started contributing to KDE?

I used to be a GNOME user because that was the default in Ubuntu, but then I decided to explore the world and see the alternatives. I read about KDE and loved it, which is a little surprising since that was KDE 4.2, and some people were still unhappy with the KDE4 series at that point. But I still loved it. Eventually, I started learning more about KDE, and I could never figure out what Nepomuk really did, so I started reading about it, and grew fascinated, but I didn't contribute. Around early 2010, I learned Qt in my spare time. My first contribution was because of a post I saw on the [Klassroom section](http://forum.kde.org/viewforum.php?f=71) of KDE forums - [Fixing KSnapshot Bugs](http://forum.kde.org/viewtopic.php?f=76&t=16626). I tried fixing one of the bugs mentioned, but failed miserably. Eventually, I found another bug in [Dolphin](http://dolphin.kde.org/), nearly refactored large parts of the codebase in an attempt to fix it. In the end, it was just a 4 line patch to Dolphin. From there I migrated to Nepomuk development and have stuck there ever since.

## How much time do you spend working on KDE?

Right now, since it is my day-job, I try to limit myself to 8 - 11 hours a day, but it varies based on many factors. Although even when I was just working on Nepomuk in my spare time I used to manage to put in around 20 hours a week. I think. It's a little hard to qualify what counts as "working on KDE".

## What keeps you motivated?

I really believe that Nepomuk has a lot of potential and I would love to see it reach its true potential. Though sometimes that's not enough, and that's when the other KDE developers come in. In May I attended the [KDE Workspace Sprint](http://vizzzion.org/blog/2012/06/next-iterations-of-the-kde-workspaces/) in Pineda de Mar, Spain. I had never attended a sprint before, and just being around so many KDE developers, having open conversations with them, playing table tennis, and hacking on stuff together was amazing. The sprint motivated me to work on Nepomuk more than anything else.

## What are some of your future goals for your involvement with KDE?

I always have big plans, but not enough time to implement all of them. Right now for the next couple of months, my primary focus is the stabilization and performance on Nepomuk. It needs to be blazingly fast, and even though we aren't just a desktop search solution, we do perform that job, and we should do it well. I have also been [trying to promote Nepomuk development](http://vhanda.in/blog/2012/07/opening-up-nepomuk-development/) and lowering the bar for entry. Apart from that I'm quite interested in KDE [Telepathy](http://community.kde.org/Real-Time_Communication_and_Collaboration) and [Plasma Active](http://plasma-active.org/) development. Though I mostly stay on the sidelines just looking at commits.

## What are some of your hobbies and interests outside of KDE?

Until recently, I used to spend most of my free time on KDE. And now that I'm working on KDE, I have been trying to find a new way to spend my free time. I mostly just read a lot of books and occasionally watch some random movies/series. And of course there are always friends to hang out with and some occasional parties.
