---
layout: post
title: "KDE and the Masters of the Universe - 0x0013"
date: 2010-10-02 00:01:00
submitter: sayakb
---

[This week](http://webbaverse.com/media/kdemu-0x0013), on [KDE and the Masters of the Universe](http://webbaverse.com/shows/kde-and-the-masters-of-the-universe), [Kopete ex-maintainer](http://kopete.kde.org/) and all around [Basket case](http://basket.kde.org/), [Matt Rogers](http://twitter.com/mattr_).

Hosts: [Klaatu](http://www.thebadapples.info/) [Guillermo A. Amaral (gamaral)](http://twitter.com/gamaral)

After show: http://webbaverse.com/media/b-sides-0x000a
