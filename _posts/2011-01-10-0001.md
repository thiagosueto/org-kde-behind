---
layout: post
title: "Patrick Spendrin"
date: 2011-01-10 00:01:00
submitter: pgquiles
---

Last week KDE 4.5.4 was released for Windows. It was a late Christmas present from the KDE on Windows team and we were immediately interested in learning more about the project. In the first interview of the "Platforms" series, Pau Garcia i Quiles talks with Patrick Spendrin, the current release manager of KDE on Windows and asks, well, everything.

## Patrick Spendrin

Name: Patrick Spendrin
Age: 29
Location: Leipzig, Germany
IRC nick: SaroEngels
Project: KDE on Windows
Blog: saroengels.blogspot.com

# Introductory Questions

## Hello Patrick, glad to talk to you. Please, tell us a bit about yourself.

I am nearly 29 years old now, living with my wife in Leipzig (Germany). I am working for KDAB as a developer on various projects.

## How and when did you get started with KDE on Windows?

I started using KDE back when I studied Physics around 2005.

Due to a programming job I had back then, I needed to switch back to Windows, but I still dreamed of having my favourite KDE applications. After hearing of the porting efforts in the pre-4.0 times, I joined the team back then.

Since I was nicely welcomed, I stayed.

## Wow, 2005! That's 6 years ago! How old is the KDE on Windows project?

KDE on Windows' origins are several projects actually: Ralf Habacker started a Cygwin-based port more than a decade ago, Holger Schröder worked on a native port of the GPL version of Qt to Windows in KDE 3 times (started in late 2002) and Jarosław Staniek developed Kexi based on the commercial Qt edition.

These efforts joined together in 2005, after Trolltech made Qt on Windows available under the GPL.

## What do you do these days in KDE on Windows?

Several tasks: I am maintaining some scripts we use to automate Windows builds of KDE, I am also doing the binary releases that can be downloaded from our website, and if I find time after that, I am also trying to fix some bugs - the last thing is not too often though.

## I can see you have a busy schedule. Are you being paid for that work or do you use your spare time?

I got paid from my company for some tasks, but normally I am not paid for it.

# Users

## Say I am a Windows user and I want to install an application. Where do I get started?

There is a detailed description on TechBase, it explains the process quite well.

To give you an idea: you download our kdewin-installer, which is a small executable that downloads and installs all required packages. In the installer you can select the packages and applications you want to have installed, and after everything is done, they work just like any other native Windows application.

## That model resembles Linux, where KDE is a platform: you need the desktop and a lot of the applications KDE provides to have a usable system. This is not the case on Windows, where the system bundles a lot of applications. Is there anything lighter if I only want a couple of applications?

Yes, this system is directly derived from Linux, and fits KDE on Windows rather well in my opinion.

There are also single application installers for many applications already, like Digikam, Okular and RKWard, just to mention a few. These are provided mostly by the authors of those applications.

## One of the most known applications in the KDE ecosystem is the Amarok player. What can you tell us about it?

Amarok was one of the applications I wanted to see running on Windows when I joined KDE.

Porting Amarok was more complex than it may seem at first sight: Amarok uses technology from the Plasma desktop and makes extensive use of Phonon. One of the most complicated things was the Phonon backend to play actual music. Now that we use VLC as the backend on Windows, most problems there are solved. We even have a standalone installer for Amarok.

Just before you asked me for this interview, I was actually sitting at fixing a last bug in Amarok.

## Many people ask about Plasma. What's the status and the plans on this? Will we be able to replace the Windows UI with Plasma?

The Plasma Desktop itself on Windows is nothing but a full screen application which always stays in the background. This also means, that it already works (more or less) now.

You can already replace your Windows "Shell" as it is called with Plasma, and you can find multiple videos of that on YouTube.

The problem we have is that a lot of system integration is still missing and needs quite some work to meet the standards.

This also means that some simple Plasmoids still don't work, because reimplementing them for Windows is much more complicated than one would think.

Other Plasmoids just work as if they would have been written for Plasma on Windows.

## How well does KDE integrate with Windows?

We already have done quite a lot there, you can have a context menu for the Windows Explorer and we have fixed several Unix specific things already.

There are still some issues we work on or that still wait to be implemented, e.g. the Plasma System Tray, Active Directory support, a wallet integration with several other applications and a lot more.

There are some really interesting tasks in it, where you can learn a lot about Windows and how operating systems in general work.

# Development

## Many of our readers are Linux users and developers. Development on the Windows world is quite different from Linux and Unix in general, isn’t it?

Windows development is really different from Linux: you have a choice of compilers, each of which behave a bit differently from each other, and each of which is not the accepted standard (contrary to what GCC is on Linux).

The most interesting problem for KDE on Windows though is that you don't have a system that provides standardized third-party libraries. That forced us to provide all those libraries ourselves, and today we manage our own packages for over 50 of them.

The different file layout with disk drives and no standard paths leads to a lot of smaller bugs, that all need to be solved.

Nonetheless, Windows works on the same computers, so if you have understood how to develop on Linux or Unix, you only have to adapt to some new things, the general development process and many tools are the same on both platforms.

And as a side note: I am also working on bringing KDevelop to Windows ;-)

## As you say, most KDE developers work on Linux and always use the GNU C compiler, which is not officially supported on Windows. What compiler does KDE on Windows use?

We now use the Windows version of GCC from the mingw-64 project. After years of only slow development we switched from the original mingw project to mingw-w64; mingw-w64 provides us with a stable and up to date GCC, and even a 64-bit version of the toolchain.

Of course, we also use the free (as in beer) compilers provided by Microsoft. They provide better results in some places, some features are missing though, like proper C99 support.

## What is your experience with GCC on Windows? Is it good enough to forget about Visual C++? So bad you’d rather stop supporting it? What’s the state of the tools?

One of the main disadvantages of the GCC toolchain on Windows is the debugger and the size of the executables. Executables from Visual C++ are a lot smaller, especially when debugging. So from a more technical point of view, one would simply choose Visual C++ and drop GCC.

On the other hand, KDE on Windows is a Free Software project, and we want to support free tools also on Windows (that's why we do KDE on Windows, right?). Which means we will keep GCC as long as it is possible at all.

I already spoke about the debuggers, one problem that sometimes arises is that we don't have Valgrind on Windows. There are some proprietary, expensive solutions, but nothing we can and want to use. This is one thing I am really missing when developing on Windows.

## The Windows world has been quite slow in adopting 64-bit operating systems and applications but they are catching up. How is KDE on Windows doing in that regard?

In the back we already have most of our 3rd party libraries on 64bit, parts of KDE itself already do build, but there are some Windows 64-bit specific problems that keep it from running.

The major problem we face is that, on Linux, a pointer can always be stored within a variable of type long. On 64-bit Windows, a variable of type long is still 32-bit wide, which means that you cannot store a pointer inside. Given that long is used so often to store a pointer, and it is even hardcoded into some protocols, we have some problems to get this right.

## Microsoft recently announced they have ported Windows and Office to the ARM architecture. Are there any plans to port KDE on Windows to non-Intel architectures?

Actually, KDE on Windows is already running on ARM architectures: Kontact Touch has already been ported to Windows CE. Many more KDE applications could get ported. I would love to see Okular there.

Microsoft has currently restricted the access to the underlying operating system in the newest Windows Phone 7. I am pretty sure that if they decide to open that again, we can easily use the infrastructure we already set up for Kontact Touch and get some more of our KDE applications running on our smartphones.

## KDE is huge, staggeringly huge if you add up the third party dependencies. How do you manage to build everything?

Well, KDE itself uses CMake which provides us with an easy cross platform build tool. This makes it really easy for us, given the amount of source code that makes up KDE.

Third-party libraries have a mix of other buildsystems though, which fit only one of our compilers or don't really care about Windows at all. So we wrote CMake buildsystems for nearly all of them. This gives us a lot of power, at a large cost in developer time. But I don't think we could have managed it otherwise.

To build all packages in the correct order, and to make it possible to automate building, we have some build scripts called emerge. We only took a bit of source code, the general idea and the name from the Gentoo tool. We developed those scripts after our own needs, and do not really have a lot in common today.

The basic idea also exists on Linux (think of kdesvn-build and build-tool) but since Windows doesn't provide us with the third-party dependencies or development tools, we also have to handle those.

# Packagers

## The way you put it, everybody in the team seems to be both a "packager" and a "software developer". Is this something you look for, of is it just the way it is?

That is mostly due to the fact that we have a very small team.

While preparing a release, most of us fix the builds for the release. After the release, everybody goes back to fixing the bugs he dislikes most, develop his own applications or just return to his work.

This probably will not change unless enough people join the project and keep doing only one thing.

On the other hand, I also find it really interesting to have the overview from fixing a bug, over patching the source tarballs, building the packages and last uploading them so everybody can install them. I got a good idea of how this process would work on Linux while trying to do a similar thing on Windows - I guess I would have never learned that if I had stayed on Linux.

## In the Linux world, many packagers know little to nothing about coding. Would you like to see people coming and only taking care of packaging so that developers could concentrate on code? Would the team be prepared to accept them?

Well, I think at the moment it would still be very hard for somebody that cannot code at all. Lots of packages need fixes that are far from trivial.

On the other hand, compared to what I learned until today, I couldn't code a lot when I joined KDE, so for somebody who is open and eager to learn while packaging, I don't think it is a big problem to have only a small knowledge of C++ or Qt.

Since we still do not assign tasks and more or less everybody is free to do what he wants, we welcome everybody to work with us.

I don't see a lot of problems with that, and I normally try to explain everything in detail (sometimes even too much?).

So, if you out there think that you are not yet ready for the coding job but the packager job suites you more, just ask! There are a lot of tasks waiting just for you

## How many developers are working on KDE on Windows?

Probably between 5-10

This is not a fixed number since some Linux developers also do fix bugs on their apps on Windows, and after all most of the code is written on Linux.

## I’m sure many of our readers want to contribute and are now thinking in becoming developers. How can they get started?

First of all, come and get in touch with us.

Either join our mailing list kde-windows, come into the #kde-windows IRC channel, or ask in the KDE Windows forum. It is much easier to give you some up to date informations if we can speak directly to you.

After that, try to build KDE and one or two favourite applications. You can ask us for some bugs or other tasks you can try to fix, if you do find some on your own.

Be aware though, that some of the seemingly easy tasks are really really hard to solve, so better ask us on the first few you try to fix.

After a while, you will get a feeling where problems come from and how to fix them.

You can also find the build instructions on TechBase in the 'Getting Started' section for Windows. We try to add the important information there, yet some information might be missing there.

# Future

## In April 2010 Microsoft announced they intend to make ports of open source software to Windows easier. Do you think it will help KDE or are you all set with emerge?

Well, unlike on Linux, we always have to rethink the way we distribute software on Windows. First we thought that a package manager is enough, today a lot of single application installers are provided already and we cannot say what the future might bring. But until then, a lot of work needs to be invested, and KDE on Windows still progresses at a very slow pace.

Things like Microsoft CoApp, a common open source build and release platform, are still rather far away from the way KDE on Windows is done.

## Let’s forget about packaging for now and get back to software. Many KDE applications and subsystems already run on Windows, yet the KDE on Windows team still says it is not stable enough. How complete is what there is and what’s left to do?

A lot of applications have been built only once or twice and never got packaged.

Applications get only fixed, built and packaged if we find somebody who is really interested in these. So we still need to find people caring about applications like KRecipes, Quanta or even Calligra Suite.

The issues all KDE applications have in common are not very visible, but some internal KDE services just stop working or some features are just not implemented under Windows because the code implementation is simply missing.

On the other hand some applications can be and are used daily and for work, so this is a bit hard to say.

## In your opinion, what are the biggest challenges KDE on Windows faces?

Most important I think is attracting new and more developers, getting a 64-bit version out and finally improving the way KDE on Windows is installed - meaning a better kdewin-installer as a package manager, single-application installers for selected packages and also a more fine grain packaging.

# Third parties

## Say I’m developing a KDE application on Linux and now I want to port it to Windows. On Linux, I just need to install the -dev or -devel packages for kdelibs and other libraries I’m using. How do I set up an equivalent development environment for KDE applications on Windows?

Well, in theory you could do the same with the kdewin-installer. There are some bugs in the buildsystem though which will not let you do that. That’s why we currently restrict building to the emerge tool.

This also makes it possible for us to have a reproducible way for packaging.

Using emerge and building everything yourself is quite a big thing in the first place but is easier to maintain than an installation with the kdewin-installer.

## Since Nokia acquired Trolltech, Qt use has exploded. Do you know if commercial software is being developed on top of KDE on Windows?

Some companies including KDAB have worked on KDE based Windows projects already.

The most known one is probably Kontact on Windows and Windows CE, but there are more I know of. I even hope to see some in the future, although I guess that KDE itself is a bit too big for most projects.

Some of the KDE technologies can probably be used for commercial applications though, and already are.

I'm sorry but I do not recall names.

# Collaboration

## KDE development is mostly Linux-centric but there are ports to other platforms such as Macintosh, BSD, Solaris, Haiku, etc. Is there any collaboration between people porting to non-Linux platforms? How different are the problems each platform faces?

Some problems are common, e.g. I know of some problems both the Oracle compiler for Solaris and the Microsoft compiler do share.

On the other hand, most of the other platforms are Unix platforms, so they don't have a problem with e.g. the file layout that we do have on Windows and Windows CE regardless of the compiler.

So all in all there is not too much collaboration, even if I am subscribed to kde-mac mailing list.

## When plans to officially support KDE4 on Windows were first discussed, I remember some people were not happy. What is the situation these days? Do developers code with cross-platform in mind?

Even if the developers might still not code totally cross platform, there is no question anymore whether KDE should have ports to other platforms.

This was the case around the time of the decision to add those "non-free" platforms.

I don't think it is possible at all for people to easily code for a platform they never use nor want to use, so I don't blame them.

When KDE on Windows becomes bigger in the future, and more contributions come from developers based on Windows, I think people will also code in a different way.

And of course, a lot of code that is broken today, has been written far before KDE 4.0.

So all in all I am very happy about the situation now.

# Personal life

## It is no secret that spouses of open source software developers generally consider we spend too much time in front of the computer in our hobby. What does your wife think of your involvement with KDE?

Well, she came with me to the Akademy in Belgium, uses KDE on Linux herself and so normally she is quite OK with it.

## Are you involved in other open source projects?

I have some small personal projects, but they all are linked somehow to KDE, so no.

## Apart from KDE, what other hobbies do you have?

In the winter, I am trying out my cross-country skiing skills and in summer I enjoy a little biking; when I was younger, I was also rowing for several years, but due to the rather small river here in Leipzig, I am not doing that often anymore.

When I find the time, I also like some crafts: crochetting, sewing and such stuff, but this directly competes with programming time...

## Where do you see yourself (and family), KDE and KDE on Windows in 5 years?

Well, I see at least two kids running around myself still working on Qt and KDE related projects.

Also I see a smoother start for KDE Platform 5 on quite a lot of platforms, mobile platforms, desktop platforms etc.

KDE on Windows will be an equal platform to the rest with more and younger developers.

# Thanks

## Is there anything you want to add?

I want to say thanks to all those people I have met in KDE in the past 4 years, that included me into their group and let me make my mistakes and get their programs running on this "weird"platform, and of course I also want to thank my wife for supporting me all those years.

## Patrick, thank you very much for your time. Congratulations on your work.
