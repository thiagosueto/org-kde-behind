---
layout: post
title: "KDE and the Masters of the Universe - 0x0011"
date: 2010-09-14 00:01:00
submitter: neverendingo
---

[This week](http://webbaverse.com/media/kdemu-0x0011), a new season of [KDE and the Masters of the Universe](http://webbaverse.com/shows/kde-and-the-masters-of-the-universe) kicks off with our good friend, [KDE e.V. Board Member](http://ev.kde.org/corporate/board.php), [Sebastian Kügler](http://satellite.vizzzion.org/blog/) (and his two chinchillas).

Hosts: [Lydia Pintscher (nightrose)](http://identi.ca/nightrose) [Guillermo A. Amaral (gamaral)](http://twitter.com/gamaral)
