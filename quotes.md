---
layout: page
title: Quotes
css-include: /css/main.css
sorted: 4
---

# Quotes

Here you will find the favourite quotes of all people in alphabetical order.

## [Adriaan de Groot]()

"Would you like a freem?" and "half zat is weggesmette geld".

## [Anne-Marie Mahfouf]()
"Don't do tomorrow what you can do today."(Ne remets pas à demain ce que tu peux faire aujourd'hui)

## [Carsten Pfeifer]()
"Feature freeze means, that everyone has a bad feeling when he changes something, almost nothing more :)"    Stephan Kulow

## [Charles Samuels]()
"I might disagree with what you have to say, but I'll defend to the death your right to say it."    Voltaire

## [Chris Schlaeger]()
"Lead, follow or get out of the way."

## [Cristian Tibirna]()
"The wise lets it go."

## [David Faure]()
"Never do today what you can postpone and do tomorrow."

## [Dirk Mueller]()
"The most reliable proof that there are extraterrestrial intelligent lifeforms out there is that nobody actually tries to get in contact with us."

"Oh no, I'm sure nobody will even dare to think of writing this kind of HTML code!"    the KHTML team, shortly before the daily "reading bug reports hour" starts.

## [Eric Laffoon]()
I could use "In a world without walls and fences who needs windows and gates?" but I like the boring one below because it means something special... "I'm part of KDE and Quanta."

## [Frerich Raabe]()
"'s macht soviel Spass wie im Regen ein Schwarzbrot essen."

## [George Staikos]()
"D'oh!"

## [Harri Porten]()
"Woodstock II, August 13, 1994. Thunderstorm. Primus on stage...."

## [Ivan E. Moore II]()
"Act like a dumbshit and they'll treat you as an equal."

## [Jono Bacon]()
"If you put your mind to it...you can do anything."

## [Konqi Konqueror]()
"If there is smoke, there could be a fire."

## [Kurt Granroth]()
"Damn the torpedoes. Full speed ahead"    Capt. David Farragut

## [Lars Knoll]()
"Only two things are infinite, the universe and human stupidity, and I'm not sure about the former."    Albert Einstein

## [Lauri Watts]()
"If there's no light at the end of the tunnel, get down there and light the darn thing yourself!"

## [Luigi Genoni]()
"Unix is the most user friendly system I know, the point is that it is really selective about who is indeed its friend."

## [Martin Jones]()
"Feature freeze means that everyone has a bad feeling when they change something, almost nothing more."    Stephan Kulow

## [Matthias Elter]()
"Your mind is like a parachute. It works best when open."

## [Matthias Ettrich]()
Variatio delectat. -- (after Cicero, "de natura deorum" 1, 9, 22)"Variety gives joy"The origin of " variatio (or varietas) delectat" is to find with Euripides (484 - 406 v. Chr.) in "Orest". There it means: "alternation is always good". First latin version supplies Cicero with the question: " Varietatene eum delectari putamus, qua caelum et terras exornatas videmus? "(Do we believe that he (i.e. God) enjoys the multicolored change, with which we see skies and earth decorated?)"

## [Michael Brade]()
"Wenn Du redest, dann muß deine Rede besser sein, als es dein Schweigen gewesen wäre." - Something like "If you talk, then your speech must be better, than it would have been your silence."

## [Michael Goffioul]()
"When I'm with you, I know tomorrow will be tomorrow".    Extracted from lyrics a friend of mine wrote for one of his songs. Fortunately, he was better in music composing than lyrics writing...

## [Michael Häckel]()
"Was net." (Slang for "I don't know")

## [Navindra Umanee]()
"The best thing you can do, is to yank offending posts as quickly as possible. And probably the ones responding to them as well. If the trolls never get to make any permanent mark, they'll get bored and go away. That's why Slashdot is full of trolls and Kuro5hin has none."    Otter, dot reader

"Don't believe the results of experiments until they're confirmed by theory."    Arthur Eddington

## [Ralf Nolden]()
"Et haett noch immer joht jange" (Slang in the Rhineland area for "It's always turned out well in the end").

## [Reggie Stadlbauer]()
"Was Du heute kannst besorgen, das verschiebe nicht auf morgen" - something like, "what you could do today, do it today and don't postpone it until tomorrow."

## [Richard Moore]()
"The difference between theory and practice, is that in theory, there is no difference between theory and practice."

## [Rik Hemsley]()
"Yes, of course we have mint choc coffee pistachio vanilla flavour."

## [Rinse de Vries]()
"If a man is considered guilty for what goes on in his mind, then gimme the electric chair for all my future crimes..."    Prince - Electric Chair

## [Sandy Meier]()
"Until he extends the circle of compassion to all living things, man will not himself find peace."    Albert Schweitzer

## [Jing-Jong Shyue]()
"Life found ways"

## [Sirtaj Singh Kang]()
"First change yourself, only then try to change the world."    MK Gandhi

## [Stefan Taferner]()
"Those who die rich are also dead." (sounds better in German) In other words: there is more in life than earning money.

## [Stefan Westerfeld]()
"Software is like sex, it's better when it's free."    Linus Torvalds

## [Stephan Kulow]()
When I was quite new to KDE I had "Du musst weiterkämpfen bis zum Umfallen auch wenn die ganze Welt nen Arsch offen hat - oder gerade deswegen" in my signature and it still comes into my mind as favorite. It translates to "you have to fight til the end even if the whole world pisses you off - or exactly for that reason"

## [Tink Bastian]()
"In order to achieve that what's worth fighting for, it maybe neccessary to lose everything else."

## [Simon Hausmann]()
"Der Mensch erfand die Atombombe, doch keine Maus der Welt würde eine Mausefalle konstruieren."(translation): "Mankind invented the atomic bomb, but no mouse would ever construct a mousetrap."    Albert Einstein

"ok, maybe my approach is over-engineered"

## [Till Adam]()
"Shall we drink a toast to absent minded friends?"    Moloko

## [Werner Trobin]()
"The last good thing written in C was Franz Schubert's Symphony No. 9."

## [Waldo Bastian]()
"Ignorance is inexcusable."

## [Wilbert Berendsen]()
"Reporter: Mr. Ghandi, what do you think of western civilisation?Ghandi: I think that would be a great idea."    (source unknown)

## [Wolfram Diestel]()
"Donu la paciencon akcepti la neshangheblajn aferojn, donu la forton ekagi pri la shangheblaj aferoj kaj la saghecon distingi ambau."Excuse me for not translating it. There are dictionaries at http://dictionaries.travlang.com ;-)

## [Zack Rusin]()
Well, pretty much all my T-Shirts have some weird slogan on them. Lately I like the ones that say "No idols, No heroes - Just pure inner strength", "To "look up to someone else is to look down on yourself"
