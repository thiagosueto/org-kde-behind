# Example

## Build instructions

```
gem install bundler jekyll
bundle install
```

## Run development

```
bundle exec jekyll serve
```

## Run production

```
bundle exec jekyll build
```

The configuration is located in `_config.yml`.

You should also change the path to the theme in Gemfile:

```
gem "jekyll", "4.0"
gem "jekyll-kde-theme", :git => 'https://invent.kde.org/websites/jekyll-kde-theme.git'
```
